class ReversiBoard:
    def __init__(self):
        self.color = {
            'Green': '\033[32mO\033[m',
            'Red': '\033[31mO\033[m'
        }
        self.turn = 'Red'
        self.status = {'Green': True, 'Red': True}
        self.border = [' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
        self.board = self.initBoard()
        self.directions = [
            [-1, -1], [-1, 1], [-1, 0], [0, -1], [0, 1], [1, 1], [1, 0], [1, -1]
        ]

    def initBoard(self):
        y1 = ['1', '.', '.', '.', '.', '.', '.', '.', '.']
        y2 = ['2', '.', '.', '.', '.', '.', '.', '.', '.']
        y3 = ['3', '.', '.', '.', '.', '.', '.', '.', '.']
        y4 = ['4', '.', '.', '.', self.color['Green'], self.color['Red'], '.', '.', '.']
        y5 = ['5', '.', '.', '.', self.color['Red'], self.color['Green'], '.', '.', '.']
        y6 = ['6', '.', '.', '.', '.', '.', '.', '.', '.']
        y7 = ['7', '.', '.', '.', '.', '.', '.', '.', '.']
        y8 = ['8', '.', '.', '.', '.', '.', '.', '.', '.']
        return [y1, y2, y3, y4, y5, y6, y7, y8]

    def drawBoard(self):
        print(' '.join(self.border))
        for line in self.board:
            print(' '.join(line))

    def changeTurn(self):
        if self.turn == 'Red':
            self.turn = 'Green'
        else:
            self.turn = 'Red'

    def getColor(self):
        return self.color[self.turn]

    def changeColor(self, current):
        return self.color['Green'] if current == self.color['Red'] else self.color['Red']

    def endGame(self):
        return True if (not self.status['Green'] and not self.status['Red']) else False

    def calculateResult(self):
        count_red = 0
        count_green = 0
        for line in self.board:
            for char in line:
                if char == self.color['Red']:
                    count_red += 1
                elif char == self.color['Green']:
                    count_green += 1
        print('End of the game. Red:', str(count_red) + ', Green:', str(count_green))
        if count_red > count_green:
            print('Red wins.')
        elif count_green > count_red:
            print('Green wins.')
        else:
            print('Draw.')

    def reverseBoard(self, x, y, direction_x, direction_y):
        while self.board[x][y] == self.changeColor(self.getColor()):
            self.board[x][y] = self.color[self.turn]
            x += direction_x
            y += direction_y

    def canReverse(self, x, y, direction_x, direction_y):
        try:
            if self.board[x][y] == '.':
                return False
            while self.board[x][y] == self.changeColor(self.getColor()):
                x += direction_x
                y += direction_y
                if self.board[x][y] == '.' or x < 0 or y < 1:
                    return False
            return True
        except IndexError:
            return False

    def findValid(self):
        def trial_move(x, y):
            for [delta_x, delta_y] in self.directions:
                try:
                    if self.board[x + delta_x][y + delta_y] == self.changeColor(self.getColor()) and \
                        self.canReverse(x + delta_x, y + delta_y, delta_x, delta_y):
                        str_move = self.border[y] + str(x + 1)
                        if str_move not in move:
                            move.append(str_move)
                except IndexError:
                    pass

        move = []
        for i in range(8):
            for j in range(9):
                if self.board[i][j] == '.':
                    trial_move(i, j)
        move.sort()
        return move

    def changeBoard(self, move):
        def go_to_direction():
            for [delta_x, delta_y] in self.directions:
                try:
                    if self.board[x + delta_x][y + delta_y] == self.changeColor(self.getColor()) and \
                        self.canReverse(x + delta_x, y + delta_y, delta_x, delta_y):
                        self.reverseBoard(x + delta_x, y + delta_y, delta_x, delta_y)
                except IndexError:
                    pass
        x = int(move[1]) - 1
        y = self.border.index(move[0])
        self.board[x][y] = self.getColor()
        go_to_direction()

    def startGame(self):
        while not self.endGame():
            self.drawBoard()
            choices = self.findValid()
            if choices:
                print('Valid choices:', ' '.join(choices))
                move = input("Player " + self.turn + ': ')
                while move not in choices:
                    print(move, ':', 'Invalid choice!')
                    print('Valid choices:', ' '.join(choices))
                    move = input("Player " + self.turn + ': ')
                self.changeBoard(move)
                self.status[self.turn] = True
            else:
                print('Player', self.turn, 'cannot play.')
                self.status[self.turn] = False
            self.changeTurn()
        self.calculateResult()
