from reversi_board import ReversiBoard


if __name__ == '__main__':
    try:
        reversi = ReversiBoard()
        reversi.startGame()
    except (KeyboardInterrupt, EOFError):
        print('Exit game. See you again.')
    except Exception:
        print('Unexpected error occurs.')